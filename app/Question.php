<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    //
    protected $guarded = [];

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }
    public function getUrlAttribute(){
        return "questions/{$this->slug}";
    }
    public function getCreatedDateAttribute(){
        return $this->created_at->diffForHumans();
    }

    public function getAnswersStyleAttribute(){
        if($this->answers_count > 0){
            if($this->best_answer_id){
                return "has-best-answer";
            }
            return "answered";
        }
        return "unanswered";
    }
    public function getFavoritesCountAttribute(){
        return $this->favourites->count();
    }
    public function getIsFavoriteAttribute(){
        return $this->favourites()->where(['user_id'=>auth()->id()])->count() > 0;
    }
//    public function getAnswerStyleAttribute(){
//        return $this->first_name." ".$this->last_name;
//    }
    /**
     * RELATIONSHIP METHODS
     */
    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function answers() {
        return $this->hasMany(Answer::class);
    }
    public function favourites(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    /**
     * HELPER FUNCTION
     */
    public function markBestAnswer(Answer $answer){
        $this->best_answer_id = $answer->id;
        $this->save();
    }
}
